// Utility to convert NCBI taxonomy ID to full taxonomy

package main

import (
    "bufio"
	"flag"
	"fmt"
	"log"
	"strings"
    "os"
    "time"

    "bitbucket.org/GottySG36/utils"
)


type Input struct {
    Files []string
}

func (i Input) String() string {
    var b strings.Builder
    var sep string
    for _, v := range i.Files {
        b.WriteString(sep)
        b.WriteString(v)
        sep = ","
    }
    return b.String()
}

func (i *Input) Set(s string) error {
    i.Files = strings.Split(s, ",")
    return nil
}

var file = &Input{}
var names = flag.String("names", "", "Names file used to build the taxonomy")
var nodes = flag.String("nodes", "", "Nodes file used to build the taxonomy")
var ids = flag.String("ids", "", "List of ids to lookup, comma seperated")
var interact = flag.Bool("interact", false, "Will prompt users for the ids to convert, one at a time")


func main() {
    flag.Var(file, "f", "File containing a list of taxonomic ids to convert, '-' to read from standard input")
	flag.Parse()

    quit := make(chan struct{})
    go utils.Spinner("Loading database", 100, quit)
	dNames, dNodes, err := loadDB(*names, *nodes)
	if err != nil {
		log.Fatalf("Error -:- loadDB : %v\n", err)
	}
    close(quit)
    time.Sleep(250 * time.Millisecond)
    fmt.Printf(" Done!\n\n")

	if *ids != "" {
		listIds := strings.Split(*ids, ",")
		for _, id := range listIds {
			tax := convertId(id, dNames, dNodes)
			fmt.Printf("ID %v : %v\n", id, tax)
		}
	}

    if fmt.Sprintf("%v", file) != "" && len(file.Files) == 1{
        var f *os.File
        if file.Files[0] != "-" {
            f, err = os.Open(file.Files[0])
        } else {
            f = os.Stdin
        }
        defer f.Close()
        if err != nil {
            log.Fatalf("Error -:- Open : %v\n", err)
        }
        r := bufio.NewScanner(f)
        for r.Scan() {
            id := r.Text()
            if id == "" {
                continue
            }
            tax := convertId(id, dNames, dNodes)
            fmt.Printf("ID %v : %v\n", id, tax)
        }
    }

	var exit bool
	if *interact {
        fmt.Println("Entering interactive mode. Type in an identifier at a time, then <Enter> (exit to leave)")
		for {
			if exit {
				break
			}
			query := getInput("ID to search? ")
			if err != nil {
				log.Fatalf("Error -:- getInput : %v\n", err)
			}

			switch query {
			case "":
				fmt.Printf("Empty query, please enter a number to search ('exit' to leave)\n")
			case "exit":
				fmt.Printf("Now leaving...\n")
				exit = true
				break
			default:
				tax := convertId(query, dNames, dNodes)
				if err != nil {
					log.Fatalf("Error -:- convertId : %v\n", err)
				}
				fmt.Printf("ID %v : %v\n", query, tax)
			}
		}
	}
}
