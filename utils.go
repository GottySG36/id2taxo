// Utility to convert NCBI taxonomy ID to full taxonomy

package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strings"
)

func getInput(greet string) string {
	r := bufio.NewScanner(os.Stdin)
	for {
		fmt.Printf("\r%v : ", greet)
		r.Scan()
		switch inp := r.Text(); inp {
		default:
			return r.Text()
		}
	}
}

var nl = regexp.MustCompile(`(^\s|\s$)`)

type node struct {
	Parent string
	Rank   string
}

type lvl struct {
	Level string
	Index int
}

var levels = map[string]lvl{
	"superkingdom": lvl{Level: "k", Index: 0},
	"phylum":       lvl{Level: "p", Index: 1},
	"class":        lvl{Level: "c", Index: 2},
	"order":        lvl{Level: "o", Index: 3},
	"family":       lvl{Level: "f", Index: 4},
	"genus":        lvl{Level: "g", Index: 5},
	"species":      lvl{Level: "s", Index: 6},
}

type taxonomy []string

func (t taxonomy) ToString() string {
	return fmt.Sprintf("%v %v %v %v %v %v %v",
		t[0], t[1], t[2], t[3], t[4], t[5], t[6])
}

func loadDB(names, nodes string) (map[string]string, map[string]node, error) {
	dNames := make(map[string]string)
	nm, err := os.Open(names)
	defer nm.Close()
	if err != nil {
		return nil, nil, err
	}
	r := bufio.NewScanner(nm)
	for r.Scan() {
		line := r.Text()
		if strings.Contains(line, "scientific name") {
			entry := strings.Split(line, "|")[:2]
			for i, _ := range entry {
				entry[i] = nl.ReplaceAllString(entry[i], "")
			}
			dNames[entry[0]] = entry[1]
		}
	}

	dNodes := make(map[string]node)
	nd, err := os.Open(nodes)
	defer nd.Close()
	if err != nil {
		return nil, nil, err
	}
	r = bufio.NewScanner(nd)
	for r.Scan() {
		line := r.Text()
		entry := strings.Split(line, "|")[:3]
		for i, _ := range entry {
			entry[i] = nl.ReplaceAllString(entry[i], "")
		}
		dNodes[entry[0]] = node{Parent: entry[1], Rank: entry[2]}
	}
	return dNames, dNodes, nil
}

func convertId(id string, dNames map[string]string, dNodes map[string]node) string {
	if id == "0" {
		return "unclassified;"
	}

	lOrg := taxonomy{"k__;", "p__;", "c__;", "o__;", "f__;", "g__;", "s__;"}
	taxid := id
	parent, ok := dNodes[taxid]
	if !ok {
		return "unclassified;"
	}
	parentId := parent.Parent

	for parentId == "2" || parentId != "1" {

		tax := dNames[taxid]
		rank := dNodes[taxid].Rank

		if t, ok := levels[rank]; ok {
			lOrg[t.Index] = fmt.Sprintf("%v__%v;", t.Level, tax)
		}

		taxid = parentId
		parentId = dNodes[taxid].Parent
	}

	return lOrg.ToString()
}
